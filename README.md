# copy-and-paste-virtualbox

#This Tutorial is about copying and pasting from host-local machine to guest VirtualBox and vise versa################

################################## How to Enable Copy and Paste in a VirtualBox Running Linux or Windows############# 
################################## Written By Aref Ahmed;E-mail:-wanak3000@gmail.com Date:-10/20/2021################  

Video Link For-This Tutorial:-
https://www.youtube.com/watch?v=8MiPmL0YaJk

-Link to Download The VirtualBox Guest Additions.iso(VBoxGuestAdditions.iso):-
https://win10repair.com/virtualbox-guest-additions-download/
Note:- After attaching this iso to your virtualbox blank optical drive under storge option, this iso file contains VboxLinuxAdditions.run for most of 64 bits linux distrobutions and the all windows(32bit&64bits)
VBoxWindowsAdditions-x86 is for windows 32bits where VBoxWindowsAdditions-amd64 is for windows 64bits.

-Installing the VirtualBox Guest Additions for Linux(Vboxlinuxadditions.run)VirtualBox Machine:-
###############################################################################################
-Example-A:- let say that the host machine is The Archlinux-64bit to The guest 64 bit MXLinux VirtualBox Machine.
-Follow the Step by Step:-
##########################
-Example-A-For ArchLinux (host) machine To The Guest VirtualBox 64bit MxLinux(Debian-base) Machine:-
####################################################################################################
-Do The Following Steps First:-
-Power-off MXLinux VirtualBox Machine Then Go To The Setting Of MXLinux VirtualBox guest machine
-Click the general-advanced tap then under share clipboard and drag&drop board from the drop down box choose the bidirectional for both shareclip and drag&drop options.
-Download- The VBoxGuestAdditions-iso- to your host macine in this case is The Archlinux from-link below:-
https://win10repair.com/virtualbox-guest-additions-download/
-In The MxLinux virtualbox guest machine Scroll down to the Storage Option under IDE-Secondary Master-Click the Optical Drive then choose the downloaded VBoxGuestAdditions.iso in the Archlinux(host) Downloads folder
- Click the start tap on your guest Mxlinux virtualbox
-In the MXLinux VirtualBox Machine then open file manager then click iso in the your Virtual optical drive it will
-Open in a file manager then right click again then click open the file manager as root 
-Right click with the mouse on the "VBoxLinuxAdditions.run" file then click open in terminal.
-After finished installing the VBox Guest Additions then reboot the MxLinux-virtualbox-guest machine.

-Example-B-Installing The VirtualBox Guest Additions For guest Windows VirtualBox machines
########################################################################################### 
-Start the guest Window VirtualBox Machine 
-Scroll down under Storage click the empty optical drive in The Guest Windows VirtualBox and then attach the downloaded VBoxGuestAdditions.iso which usually exist in your Downloads Folder of your host machine. 
"The VBoxGuestAdditions.iso is compatible for Windows 10,8 and 7 for both 64 bit and 32 bit. The executable file for windows 64bits 7 ,8 and 10 is the file name "VBoxWindowsAdditions-amd64",where the executable file for windows 32bits   of 7,8 and 10 is the file name "VBoxWindowsAdditions-x86".

-Right click on the "VBoxWindowsAdditions.exe" for 32bits or the 64bits then click run as adminstrator 
-Reboot your guest Window VirtualBox Machine after it finishes installing.
-Start again your guest window VirtualBox Machine then test with copy and paste from your host machine to your guest VirtualBox Machine and vise vera.

1-Important Note:-
##################
-The above A example steps works for Archlinxus host machine to the guest virtualbox machine of Mxlinux.
-The above B example steps works for any operating host system to the guest windows virtualbox machine for windows 10,8 and windows 7 32 bit or 64 bits. 
-VBoxWindowsAdditions-x86 is for windows 32bits where VBoxWindowsAdditions-amd64 is for windows 64bits.
####################################################################################################################


